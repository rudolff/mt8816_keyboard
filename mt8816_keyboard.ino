#include <PS2KeyRaw.h>
#define AX0 4
#define AX1 5
#define AX2 6
#define AX3 7
#define AY0 8
#define AY1 9
#define AY2 10
#define RSTMT 14
#define CSMT  15
#define DATMT 16
#define STBMT 17
#define DATAPIN 2
#define IRQPIN  3

#define POS(x, y) (x | (y << 4))
#define NO_KEY 255

struct key {
    uint8_t scancode;
    uint8_t coords;
  };

key keymap[] = {
  {4,  POS(5, 0)}, // F3
  {5,  POS(3, 0)}, // F1
  {6,  POS(4, 0)}, // F2
  {12, POS(6, 0)}, // F4
  {13, POS(0, 1)}, // TAB
  {17, NO_KEY},    // L_ALT
  {18, POS(8, 0)}, // L_SHIFT 
  {20, POS(9, 0)}, // L_CTRL
  {21, POS(1, 6)}, // Q
  {22, POS(1, 2)}, // 1
  {26, POS(2, 7)}, // Z 
  {27, POS(3, 6)}, // S
  {28, POS(1, 4)}, // A 
  {29, POS(7, 6)}, // W 
  {30, POS(2, 2)}, // 2 
  {31, NO_KEY}, // L_WIN
  {33, POS(3, 4)}, // C
  {34, POS(0, 7)}, // X
  {35, POS(4, 4)}, // D
  {36, POS(5, 4)}, // E
  {37, POS(4, 2)}, // 4
  {38, POS(3, 2)}, // 3
  {39, NO_KEY},    // R_WIN
  {41, POS(7, 7)}, // SPACE
  {42, POS(6, 6)}, // V
  {43, POS(6, 4)}, // F
  {44, POS(4, 6)}, // T
  {45, POS(2, 6)}, // R
  {46, POS(5, 2)}, // 5
  {49, POS(6, 5)}, // N
  {50, POS(2, 4)}, // B
  {51, POS(0, 5)}, // H
  {52, POS(7, 4)}, // G
  {53, POS(1, 7)}, // Y
  {54, POS(6, 2)}, // 6
  {58, POS(5, 5)}, // M
  {59, POS(2, 5)}, // J
  {60, POS(5, 6)}, // U
  {61, POS(7, 2)}, // 7
  {62, POS(0, 3)}, // 8
  {65, POS(4, 3)}, // ,<
  {66, POS(3, 5)}, // K
  {67, POS(1, 5)}, // I
  {68, POS(7, 5)}, // O
  {69, POS(0, 2)}, // 0
  {70, POS(1, 3)}, // 9
  {73, POS(6, 3)}, // .>
  {74, POS(7, 3)}, // /?
  {75, POS(4, 5)}, // L
  {76, POS(3, 3)}, // ;:
  {77, POS(0, 6)}, // P
  {78, POS(5, 3)}, // -_
  {82, NO_KEY}, // '"
  {84, POS(3, 7)}, // [{
  {85, NO_KEY}, // =+
  {88, NO_KEY}, // CS_LOCK
  {89, POS(9, 0)}, // R_SHIFT
  {90, POS(2, 1)}, // ENTER
  {91, POS(5, 7)}, // ]}
  {93, POS(4, 7)}, // \|
  {102, POS(3, 1)}, // BACKSPACE
  {105, POS(1, 2)}, // NUM 1
  {107, POS(4, 2)}, // NUM 4
  {108, POS(7, 2)}, // NUM 7
  {112, POS(0, 2)}, // NUM 0
  {113, POS(6, 3)}, // NUM .
  {114, POS(2, 2)}, // NUM 2
  {115, POS(5, 2)}, // NUM 5
  {116, POS(6, 2)}, // NUM 6
  {117, POS(0, 3)}, // NUM 8
  {118, POS(0, 0)}, // ESC
  {119, NO_KEY}, // NUM LOCK
  {120, NO_KEY}, // F11
  {121, POS(3, 3)}, // NUM +
  {122, POS(3, 2)}, // NUM 3
  {123, POS(5, 3)}, // NUM -
  {124, NO_KEY}, // NUM *
  {125, POS(1, 3)}, // NUM 9
  {126, NO_KEY}, // SC LOCK
};

volatile boolean d = HIGH;
uint8_t table[128];

void initTable()
{
  memset(table, NO_KEY, 128);
  for(uint8_t i = 0; i < (sizeof(keymap)/sizeof(key)); i++)
  {    
    table[keymap[i].scancode] = keymap[i].coords; 
  }
}

PS2KeyRaw keyboard;
void setup() {

  initTable();
  //Serial.begin(115200);
  //Инициализация портов
  pinMode(19, OUTPUT);
  pinMode(AX0, OUTPUT);   //AX0
  pinMode(AX1, OUTPUT);   //AX1
  pinMode(AX2, OUTPUT);   //AX2
  pinMode(AX3, OUTPUT);   //AX3
  pinMode(AY0, OUTPUT);   //AY0
  pinMode(AY1, OUTPUT);   //AY1
  pinMode(AY2, OUTPUT);   //AY2
  pinMode(RSTMT, OUTPUT); //RES
  pinMode(CSMT, OUTPUT);  //CS
  pinMode(DATMT, OUTPUT); //DAT
  pinMode(STBMT, OUTPUT); //STB
  //Инициализация клавиатуры
  keyboard.begin( DATAPIN, IRQPIN );
  //Инициализация MT8816
  SetAddr(0);
  digitalWrite(RSTMT, LOW);
  digitalWrite(CSMT, LOW);
  digitalWrite(DATMT, LOW);
  digitalWrite(STBMT, LOW); //инициализация
  digitalWrite(CSMT, HIGH); //выбор чипа
  digitalWrite(RSTMT, HIGH);
  digitalWrite(RSTMT, LOW);  //сброс
  digitalWrite(CSMT, LOW);
}
void SetAddr(uint8_t addr) {
  digitalWrite(AX0,bitRead(addr,0));
  digitalWrite(AX1,bitRead(addr,1));
  digitalWrite(AX2,bitRead(addr,2));
  digitalWrite(AX3,bitRead(addr,3));
  digitalWrite(AY0,bitRead(addr,4));
  digitalWrite(AY1,bitRead(addr,5));
  digitalWrite(AY2,bitRead(addr,6));
}
void SetKey(boolean data){
   digitalWrite(CSMT, HIGH); //выбор чипа
   digitalWrite(STBMT, HIGH); //строб on
   digitalWrite(DATMT, data); //данные
   digitalWrite(STBMT, LOW); //строб off    
   digitalWrite(CSMT, LOW);   
}
void loop()
{
  if( keyboard.available() ){
    int c = keyboard.read(); //чтение кода

    //Serial.println(c);
    switch (c) {
      case 0xE0:              //если считался префикс 0xE0
        break;
      case 0xF0:              //если считался префикс 0xF0 (отпускание клавиши)
        d = LOW;
        break;
      default:
        if(table[c] != NO_KEY)
        {
          SetAddr(table[c]);
          SetKey(d);
        }
        d = HIGH;
    }
  }
}
